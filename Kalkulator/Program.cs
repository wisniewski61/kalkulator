﻿using Kalkulator.ExpressionHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator
{
    class Program
    {
        static void Main(string[] args)
        {
            IExpressionHandler firstHandler = getFirstHandler();
            string expression = "2-2";
            OperationModel om = new OperationModel(expression[1].ToString(), int.Parse(expression[0].ToString()), int.Parse(expression[2].ToString()));

            try
            {
                Console.WriteLine("Result: " + firstHandler.execute(om));
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }

        static IExpressionHandler getFirstHandler()
        {
            IExpressionHandler add = new AddictionHandler();
            IExpressionHandler subtract = new SubtractionHandler();

            add.setNext(subtract);

            return add;
        }
    }
}
