﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator.ExpressionHandlers
{
    public interface IExpressionHandler
    {
        int execute(OperationModel operation);
        void setNext(IExpressionHandler handler);
    }
}
