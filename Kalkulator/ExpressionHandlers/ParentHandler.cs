﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator.ExpressionHandlers
{
    public abstract class ParentHandler : IExpressionHandler
    {
        protected IExpressionHandler nextHandler;
        public abstract int execute(OperationModel operation);

        protected int nextHandlerOperation(OperationModel operation)
        {
            if (nextHandler != null)
                return nextHandler.execute(operation);
            throw new Exception("Unknown operation");
        }

        public void setNext(IExpressionHandler handler)
        {
            nextHandler = handler;
        }
    }
}
