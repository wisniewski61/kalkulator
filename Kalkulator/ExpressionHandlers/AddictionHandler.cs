﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator.ExpressionHandlers
{
    class AddictionHandler : ParentHandler
    {
        public override int execute(OperationModel operation)
        {
            if (operation.OPERATOR.Equals("+"))
                return operation.FIRST_VALUE + operation.SECOND_VALUE;
            return nextHandlerOperation(operation);
        }
    }
}
