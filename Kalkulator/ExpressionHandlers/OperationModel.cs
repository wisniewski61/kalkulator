﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator.ExpressionHandlers
{
    public class OperationModel
    {
        public string OPERATOR;
        public int FIRST_VALUE;
        public int SECOND_VALUE;

        public OperationModel(string operatorStr, int firstValue, int secondValue)
        {
            OPERATOR = operatorStr;
            FIRST_VALUE = firstValue;
            SECOND_VALUE = secondValue;
        }
    }
}
